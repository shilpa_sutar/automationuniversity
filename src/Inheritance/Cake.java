package Inheritance;

public class Cake {


    //creating constrictor//


    Cake(String flavour) {
        System.out.println(" Below are Cake Details");

      this.flavour= flavour;
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }



    public int price;
    public String flavour;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFlavour() {
        return flavour;
    }

    public void setFlavour(String flavour) {
        this.flavour = flavour;
    }
}
