package Inheritance;

public class TasteTester

{

    public static void main(String args[])
    {

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Cake cake = new Cake("vanilla");


        cake.setFlavour(" vanilla");
        cake.setPrice(600);

        System.out.println("The Flavour of cake is    --->  " + " "  +cake.getFlavour());
        System.out.println("The Price of cake is      --->  " + " "  +cake.getPrice());


        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        BirthdayCake birthdayCake= new BirthdayCake(" hhh");


        birthdayCake.setFlavour("butterscotch");
        birthdayCake.setPrice(550);

        System.out.println("The flavour of birthday cake is  ---> " + " "  +birthdayCake.getFlavour());
        System.out.println("The Price of birthday cake is    --->  " + " "  +birthdayCake.getPrice());


        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        WeddingCake weddingCake= new WeddingCake(" vanilla");



        weddingCake.setFlavour("vanilla");
        weddingCake.setPrice(350);

        System.out.println("The flavour of birthday cake is   ---> " + " "  +weddingCake.getFlavour());
        System.out.println("The Price of birthday cake is     --->  " + " "  +weddingCake.getPrice());





    }
}
