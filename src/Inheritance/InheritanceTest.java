package Inheritance;

public class InheritanceTest  {

    public static void main(String args[]){

        Person person =new Person();

        Employee employee= new Employee();

        person.setName("Shilpa");

        employee.setDept("APP QA");
        employee.setAge(29);


        System.out.println(person.getName() + " " +employee.getDept() + " "  +employee.getAge());
    }

}
