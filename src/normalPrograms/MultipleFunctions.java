package normalPrograms;

import java.util.Scanner;

public class MultipleFunctions {

    public static void reverse() {

        System.out.println("Enter string to reverse:");

        Scanner read = new Scanner(System.in);
        String str = read.nextLine();

        StringBuilder sb = new StringBuilder();

        for (int i = str.length() - 1; i >= 0; i--) {
            sb.append(str.charAt(i));
        }

        System.out.println("Reversed string is:");
        System.out.println(sb.toString());
    }


    public static void tables() {

        Scanner s = new Scanner(System.in);
        System.out.print("Enter number:");
        int n = s.nextInt();
        for (int i = 1; i <= 10; i++) {
            System.out.println(n + " * " + i + " = " + n * i);
        }
    }

    public static void factorial() {
        int m, c, fact = 1;

        System.out.println("Enter an integer to calculate it's factorial");
        Scanner in = new Scanner(System.in);

        m = in.nextInt();

        if (m < 0)
            System.out.println("Number should be non-negative.");
        else {
            for (c = 1; c <= m; c++)
                fact = fact * c;

            System.out.println("Factorial of " + m + " is = " + fact);
        }
    }

    public static void main(String[] args)

    {

        reverse();
        factorial();
        tables();


    }
}