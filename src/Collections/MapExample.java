package Collections;

import org.w3c.dom.ls.LSOutput;

import java.util.HashMap;
import java.util.Map;

public class MapExample<string>
{
    public static void main(String args[]){

        Map colormap= new HashMap();

        colormap.put(123,"QA");
        colormap.put(234,"Developer");
        colormap.put(323,"SRE");
        colormap.put(3432," tom");
        colormap.put(9090 ,"HR");
        System.out.println(colormap.get(323));
        System.out.println(colormap.size());
        System.out.println(colormap);
        System.out.println(colormap.entrySet());
        System.out.println(colormap.remove(123));
        System.out.println(colormap.size());
        System.out.println(colormap.keySet());
        System.out.println(colormap.values());




    }
}
