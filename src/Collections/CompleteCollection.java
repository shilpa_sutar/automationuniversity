package Collections;

import java.util.*;
import java.util.concurrent.Callable;

public class CompleteCollection {

    public static void main(String args[]) {
        //Collection in generic//
       /* Collection c = new ArrayList();

        c.add(22);
        c.add(11);
        c.add("Shilpa");
        c.add(2.3);
        c.remove(22);

        for (Object o : c) {
            System.out.println(o);
        }
        System.out.println("=========================================");
        //Collection in generic//
        Collection<Integer> d = new ArrayList();

        d.add(22);
        d.add(11);
        d.add(45);
        d.add(23);

        for (Object o : d) {
            System.out.println(o);
        }*/


        //sorting example//
        List<Integer> c = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i <= 100; i++) {
            c.add(r.nextInt(1000));
        }

        Collections.sort(c);
        Comparator numComp = new NumComp();
        Collections.sort(c, numComp);
        for (int o : c) {
            System.out.println(o);

        }

        //


    }
}

class NumComp implements Comparator<Integer> {

    public int compare(Integer i1, Integer i2) {
        int diff = 0;
        if (i1 % 10 > i2 % 10) {
            diff = 1;
        } else if (i1 % 10 < i2 % 10) {
            diff = -1;
        }
        return diff;
    }
}
