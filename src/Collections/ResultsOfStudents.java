package Collections;

import java.util.HashMap;
import java.util.Map;

public class ResultsOfStudents {


    public static void main(String[] args) {
        // Mapping string values to int keys for map1
        // Mapping string values to int keys for map2
        Map<String, Integer> map1 = Grade1();
        Map<String, Integer> map2 = Grade2();


        for (Map.Entry<String, Integer> entry1 : map1.entrySet()) {
            String key = entry1.getKey();
            Integer value1 = entry1.getValue();
            Integer value2 = map2.get(key);
            if (value1 >= value2)
                System.out.println(key + " scored highest result with marks " + value1);
            else
                System.out.println(key + " scored highest result with marks " + value2);
        }
    }

    private static Map<String, Integer> Grade1() {
        Map<String, Integer> map1 = new HashMap<String, Integer>();
        map1.put("Shilpa", 60);
        map1.put("Swapnil", 89);
        map1.put("Sumeet", 99);
        map1.put("Prince", 80);
        map1.put("Barkha", 89);

        return map1;
    }

    private static Map<String, Integer> Grade2() {
        // Mapping string values to int keys for map2
        Map<String, Integer> map2 = new HashMap<String, Integer>();
        map2.put("Shilpa", 62);
        map2.put("Swapnil", 80);
        map2.put("Sumeet", 94);
        map2.put("Prince", 55);
        map2.put("Barkha", 90);
        return map2;

    }
}



