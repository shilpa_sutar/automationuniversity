package Abstractionwithconstructor;


abstract class BBase {

    public String name;
    public int age;

    BBase(String name, int age) {

        this.name = name;
        this.age = age;
    }

    BBase() {
    }

    abstract void fun();
}
