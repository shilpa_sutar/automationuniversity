package Abstractionwithconstructor;

public class Derived extends BBase {

    public int empId;

    public Derived(String name, int age, int empId) {
        super(name,age);
        this.empId = empId;
    }

    void fun() {
        System.out.println(name+" "+age+ " "+empId);
    }


}



