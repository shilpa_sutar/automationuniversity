package AbstractionAssignment;

public abstract class Animal {
    private String name;

    public abstract void makesound();

    public void eat() {

        System.out.println("eat method from animal class");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
