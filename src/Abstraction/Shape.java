package Abstraction;

public abstract class Shape {

    public abstract double CalculateArea();

    public void print()
    {
        System.out.println("This is non abstract method");
    }
}
