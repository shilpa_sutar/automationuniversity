package Abstraction;

public class Book implements product {


    private double Price;
    private String Name;
    private String Color;
    private String Author;
    private int Pages;


    @Override
    public double getPrice() {
        return this.Price;
    }

    @Override
    public void setPrice(double Price) {
        this.Price = Price;
    }

    @Override
    public String getName() {
        return this.Name;
    }

    @Override
    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String getColor() {
        return this.Color;
    }

    @Override
    public void setColor(String Color) {
        this.Color = Color;
    }


}
