package Abstraction;



public class Circle extends Shape {


    private double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Circle(double radius){
        setRadius(radius);



    }

    @Override
    public double CalculateArea() {
        return (2*radius);
    }
}
